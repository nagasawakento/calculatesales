package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
		
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	
	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	
	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	
	
	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	//private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	//private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String FILE_NAMBER_ERROR = "売上ファイル名が連番になっていません";
	private static final String MONER_OVER_10MM = "合計金額が10桁を超えました";
	private static final String FORMAT_ERROR = "のフォーマットが不正です";
	private static final String CORD_ERROR = "の支店コードが不正です";
	//課題追加
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";
	private static final String BRANCH_DEFINITION = "支店定義";
	private static final String COMMODITY_DEFINITION = "商品定義";
	private static final String COMMODITY_CORD_ERROR = "の商品コードが不正です";
	
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		
		//エラー処理↓	
		
		//コマンドライン引数が1つ設定されていなかった場合エラーが起こる
		if(args.length != 1) {					
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		
		// 支店コードと支店名を保持するMap名前をbranchNames
		Map<String, String> branchNames = new HashMap<>();		
		
		// 支店コードと売上金額を保持するMap名前をbranchSales
		Map<String, Long> branchSales = new HashMap<>();	
		
		//商品コードと商品名を保持するMap名前をmerchandiseNames
		Map<String, String> merchandiseNames = new HashMap<>();
		
		//商品コードと売上金額を保持するMap名をmerchandiseSales
		Map<String, Long> merchandiseSales = new HashMap<>();
		
		

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}",
				BRANCH_DEFINITION)) {
			return;
		}
		
		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, merchandiseNames, merchandiseSales, "^[A-Za-z0-9]{8}",
				COMMODITY_DEFINITION)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		
		//args[0]はC:\Users\nagasawa.kento\Desktop\売上集計課題のこと
		//   .listFiles()は指定したディレクトリに含まれるファイルやディレクトリを値として返すメソッド
		File[] files = new File(args[0]).listFiles();	  
		
		//売上ファイルの情報だけをrcdFilesに入れる。
		List<File> rcdFiles = new ArrayList<>();		

		for (int i = 0; i < files.length; i++) {
			
			// ↓    .isFile()はファイルかどうかを判断するメソッド
			//       "^[0-9]{8}.rcd$"は正規表現
			//       .matches()は()の中の正規表現をクリアしているか判断するメソッド
			if(files[i].getName().matches("^[0-9]{8}+\\.rcd$") && files[i].isFile()) {

				// rcdFilesには今args[0]に保存されている^[0-9]{8}+\\.rcd$の正規表現をクリアしたファイル名の値が入っている
				rcdFiles.add(files[i]);		 	
			}
		}
		
		//エラー処理↓	
			
		//.sort()は指定されたリストが自然順序になっているかを判別するメソッド
		Collections.sort(rcdFiles);			
		
		for(int i =0; i < rcdFiles.size() -1; i++) {
			
			//.getName()ファイル、ディレクトリの名前を取得するメソッド
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));	
			
			if((latter - former) != 1) {
				System.out.println(FILE_NAMBER_ERROR);
				return;
			}	
		}	
		
		//BufferedReaderはテキストファイルを読み込むAPI
		//brOutはBufferedReader内の変数名
		//nullでbrOutのボックスをきれいにした
		BufferedReader brOut = null;			

		for (int i = 0; i < rcdFiles.size(); i++) {

			try {

				/*	codeSalesは、FileReader内の変数名
			 	ここではcodeSalesにrcdFiles（正規表現をクリアしたファイル名の値）が入っている    */	
				FileReader codeSales = new FileReader(rcdFiles.get(i));

				// brOutにrcdFiles（正規表現をクリアしたファイル名値）が入っているcodeSalesの中の値を入れている
				brOut = new BufferedReader(codeSales);

				//ここではArrayList<String>に新しく作ったcodeSalesListという変数名（入れ物）
				ArrayList<String> codeSalesList = new ArrayList<String>();

				//ここでString型のbrOutLinesという変数名を定めた（入れ物）
				String brOutLines;

				/*	whileは繰り返し文
				brOutLinesに繰り返し文でbrOut {rcdFiles（正規表現をクリアしたファイル名の値）が入っているcodeSalesの値を入れている}
				の値を.readLine()メソッドで一文ずつ入れている。       	*/
				while((brOutLines = brOut.readLine()) != null) {

					//codeSalesListに.add()メソッドでbrOutLinesの値を入れ、リストを作成している。
					codeSalesList.add(brOutLines);
				}
				
				//エラー処理↓
				
				//売上ファイルの中が3行になっているか確認している
				if(codeSalesList.size() != 3) {		
					
					System.out.println(rcdFiles.get(i).getName() + FORMAT_ERROR);
					return;
				}

				//エラー処理↓
				
				if(!branchSales.containsKey(codeSalesList.get(0))) {
					
					System.out.println((rcdFiles.get(i).getName() + CORD_ERROR));
					return;
				}
				
				//追加課題コード
				if(!merchandiseSales.containsKey(codeSalesList.get(1))) {
					
					System.out.println((rcdFiles.get(i).getName() + COMMODITY_CORD_ERROR));
					return;
				}
				//エラー処理↓
				
				if(!codeSalesList.get(2).matches("^[0-9]*$")) {
					
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				
				long fileSale = Long.parseLong(codeSalesList.get(2));		//codeSalesList.get(1)の値をlong型に変換している
				
				/*  ↓  売上ファイルから読み込んだ売上金額を 加算 して、Mapに追加するには既にMapにある売上金額を 
				取得 する必要があります。Mapから値を取得する際には、get というメソッドを使用します。このメソッドは、
		  		引数に Key を指定することで、Valueが取得できます。Keyには支店コード を入れているため、売上ファイル
		 		の1行目に記載されている支店コードを引数として設定します。 */
				long saleAmountBranch = branchSales.get(codeSalesList.get(0)) + fileSale;
				
				long saleAmountMerchandise = merchandiseSales.get(codeSalesList.get(1)) + fileSale;
				
				//エラーコード↓
				
				//ここではsaleAmountの金額が10桁を超えたときエラーが発生する
				if(saleAmountBranch >= 10000000000L) {		
					System.out.println(MONER_OVER_10MM);
					return;	
				}
				
				//商品定義追加コード
				if(saleAmountMerchandise >= 10000000000L) {		
					System.out.println(MONER_OVER_10MM);
					return;	
				}

				branchSales.put(codeSalesList.get(0), saleAmountBranch);
				
				merchandiseSales.put(codeSalesList.get(1), saleAmountMerchandise);
			
				
				//IOExceptionはファイルが存在しなかったり,書き込めなかったりしたときに出るエラーコード
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);          
				return ; 								   	
			
			} finally {
			
				// ファイルを開いている場合
				if(brOut != null) { 		
					try {
						// ファイルを閉じる
						brOut.close();		
					
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		
		// 支店別集計ファイル書き込み処理
		
		//配列の0番目の要素なのでargs[0]となっている
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {					
			return;			
		}
		
		//商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, merchandiseNames, merchandiseSales)) {					
			return;	
		}
	}
	
	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> variousCodeNames,
			Map<String, Long> variousCodeSales, String regular, String definitionFileName) {
		
		//BufferedReaderはテキストファイルを読み込むAPI ,  nullでbrの中を綺麗にした
		BufferedReader br = null;   
		try {
			
			//pathはファイルのパス,fileNameはファイルの名前
			File file = new File(path, fileName);		
			
			//エラー処理↓	
			
			//.exists()は対象のファイル,ディレクトリが存在するか判断するメソッド
			if(!file.exists()) {		
				System.out.println(definitionFileName + FILE_NOT_EXIST);
				return false;	
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			// 一行ずつ読み込む.readLine()メソッド
			while((line = br.readLine()) != null) {	
			
				//この２文でreadFileの中を , で分けてitemsの中に読みだせるようにしている
				//今回items[0]は001などのコードitems[1]は東京支店などが出てくるようになっている
				String[] items = line.split(",");	
				
				//エラー処理↓
				
				// itemsの要素数が2つ出なかったときとitems[0]の要素 が"^[0-9]{3}"の正規表現をクリアしているか確認している 
				if((items.length != 2) || (!items[0].matches(regular))){   
					
					System.out.println(definitionFileName + FILE_INVALID_FORMAT);
					return false;
				}	
				
				//ここでbranchNamesというマップの変数名にキー,バリューを追加した
				//branchNamesは、支店コード,支店名をマップに保持している
				variousCodeNames.put(items[0], items[1]);		

				//ここでbranchSalesというマップの変数名にキー,バリューを追加した
				//branchSalesは、支店コード,売上金額をマップに保持している
				variousCodeSales.put(items[0], 0L);
				
			}
		
			//IOExceptionはファイルが存在しなかったり,書き込めなかったりしたときに出るエラーコード
			} catch(IOException e) {						  
				System.out.println(UNKNOWN_ERROR);         
			
				//booleanなのでreturnするのは真偽値
				return false; 								  
			
			} finally {
			
				// ファイルを開いている場合
				if(br != null) {		
					try {
						// ファイルを閉じる
						br.close();		
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
							
						//booleanなのでreturnするのは真偽値
						return false;		
					}
				}
			}
			//booleanなのでreturnするのは真偽値
			return true;		
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> variousCodeNames,Map<String, Long> variousCodeSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		//BufferedWriterはテキストファイルを書き込むAPI
		BufferedWriter br = null;   

		try {
			File writerBrFile = new File(path,fileName);
			FileWriter writerFrFile = new FileWriter(writerBrFile);
			br = new BufferedWriter(writerFrFile);

			//keyという変数にvariousCodeSales内のkey値を入れる繰り返し処理を行った
			for(String key : variousCodeSales.keySet()) {		
				 
				String branchName = variousCodeNames.get(key);
			
				//moneyというLong型の変数の中にvariousCodeSales.get(key)でbranchSales内のバリュー値をいれた
				Long money = variousCodeSales.get(key);			
				
				br.write(key + "," + branchName + "," + money);
				br.newLine();
			}			
					
		//IOExceptionはファイルが存在しなかったり,書き込めなかったりしたときに出るエラーコード
		} catch(IOException e) {                        	  
			System.out.println(UNKNOWN_ERROR);          
			return false;   
					
		} finally {
			
			// ファイルを開いている場合
			if(br != null) {			
						
				try {
					// ファイルを閉じる
					br.close();		
							
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
									
					//booleanなのでreturnするのは真偽値		
					return false;			
				}
			}
		}
		//booleanなのでreturnするのは真偽値
		return true;		
		
	}
}